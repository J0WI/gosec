module gitlab.com/gitlab-org/security-products/analyzers/gosec/v2

require (
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.9.1
	gitlab.com/gitlab-org/security-products/cwe-info-go v1.0.1
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a
)

go 1.13
